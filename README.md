# Technical Test Loris Trouvé

## Description

This is the code of the technical test of Loris Trouvé at Maison Du Monde.

The test contains 3 files :

- Main.py : As the name says, it's main file of the folder. This file manages all other files. Also, the file manages the argument parser.
- SparsingArray.py : This file contains the resolution of the problem : https://www.hackerrank.com/challenges/sparse-arrays/problem
- Dockerfile : A Dockerfile is a text file describing the different steps allowing you to start from a database to go to a functional application. Docker reads the instructions you put in the Dockerfile to automatically create the required image

The problem :

There is a collection of input strings and a collection of query strings. For each query string, determine how many times it occurs in the list of input strings. Return an array of the results.
The result is intentionally a dictionary, not an array. 

## Installation

This app run with Python 3.7, Docker 20.10.0

## Usage

To run a project, simply clone this repo, and build and run
the Docker image with the following commands:

```shell
docker build . -t test_mdm
docker run -t -p 5000:5000 test_mdm abc,ab
```

Finally, just go to a browser (I only tested Chrome) and try this url:

```chrome
http://0.0.0.0:5000/
```

From there you have 2 options:

- Test the swagger at this url : http://0.0.0.0:5000/

- Raw way : you can get the result (json) with this url : http://0.0.0.0:5000/query/abc,ab by specifying instead of "abc,ab" your string


## Explanations
- ### Main
This file contains Flask app (with route), Swagger with Flask Restplus and one class which consume the file SparseArray.py

- ### Sparsing Array

This file contains 1 class MatchingString with 2 params (name : queries and string). 
The code has been commented.


All the constraints are active here : 
```python
if (1 <= len(queries) <= 1000) and (1 <= len(strings) <= 1000) and (max(strings_len) <= 20) and (max(query_len) <= 20):
```

We have add in code the manage of error of space before/after string with this : 

```python
validation = validation.lstrip().rstrip()
```

- ### Dockerfile

I have define the version of library's python. 
The versions have been defined to add robustness.
I voluntarily defined the  Werkzeug version because an error arrived with higher versions.


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.
