class MatchingString(object):

    def __init__(self):
        pass


    def MatchingString(self, queries, strings):
        '''
        :param queries: String, an array of query strings
        :param strings: String, an array of strings to search
        :return: Dict, array of results for each query
        '''

        #initializes the return dictionary
        return_dict = {i: 0 for i in queries}

        #Count number of character in lists
        strings_len_character = [len(i) for i in strings]
        query_len_character = [len(i) for i in queries]

        #Constraints
        if (1 <= len(queries) <= 1000) and (1 <= len(strings) <= 1000) and (max(strings_len_character) <= 20) and (max(query_len_character) <= 20):

            for validation in strings:

                #Remove spaces before/after string
                validation = validation.lstrip().rstrip()

                if validation in return_dict.keys():

                    return_dict[validation] = return_dict[validation] + 1

            return return_dict

        else:
            print("Constraints error")
