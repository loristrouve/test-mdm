import SparsingArray
import os
from flask import Flask
from flask_restplus import Api, Resource
import argparse

flask_app = Flask(__name__)
api = Api(app=flask_app, version="1.0", title="Swagger API for Sparsing Array")

@api.route('/query/<string:input>', methods=['GET'])
class resultat(Resource):

    def get(self, input):

        # get query variable via swagger or route
        query = input.split(",")

        return SparsingArray.MatchingString().MatchingString(query, strings)

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("strings")
    args = parser.parse_args()

    strings = args.strings.split(",")

    flask_app.run(debug=True, host='0.0.0.0')
