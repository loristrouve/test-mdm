FROM python:3.7
WORKDIR /maisondumonde
COPY . .
RUN pip install Flask==1.1.2 flask_restplus==0.13.0 Werkzeug==0.16.1 argparse==1.1
ENTRYPOINT ["python3", "main.py"]
